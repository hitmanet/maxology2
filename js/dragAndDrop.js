const preventDefaults = (e) =>{
    e.stopPropagation()
    e.preventDefault()
}

const drop = (e) =>{
    e.stopPropagation()
    e.preventDefault()
    const { files } = e.dataTransfer
    uploadFiles(files)
}
const uploadFiles = (files) =>{
    const element = document.querySelector('.owl-item.active')
    const childrenImg = element.childNodes[0].childNodes[5]
    for (let file of files) {
        // const div = document.createElement('div')
        // div.classList.add('prof')
        // const img = document.createElement('img')
        childrenImg.file = file
        const reader = new FileReader()
        reader.onload = ((aImg) => { return function(e) { aImg.src = e.target.result } })(childrenImg)
        reader.readAsDataURL(file)
        // div.appendChild(img)
        // $('.owl-carousel').owlCarousel('add', div).owlCarousel('update')
    }
}


const dropArea = document.querySelector('#drop-area')
dropArea.addEventListener("dragenter", preventDefaults, false);
dropArea.addEventListener("dragover", preventDefaults, false);
dropArea.addEventListener("drop", drop, false)





