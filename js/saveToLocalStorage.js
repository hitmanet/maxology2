
console.log('here')
const submitBtn = document.querySelector('#submitBtn')
const formChilds = document.querySelectorAll('[name]')
const elements = Object.values(formChilds).filter(value => {
    if (value.localName === 'input' || value.localName === 'textarea'){
        return value
    }
})
for (let el of elements){
    let values
    el.onchange = function(){
        values = getItemInLocalStorageByName(el.name, el.value)
        if (values !== null){
            for (let key in values){
                document.querySelector(`[name=${key}]`).value = values[key]
            }
        }
    }
}
submitBtn.onclick = () =>{
    saveFormToLocalStorage(elements)
}

const getItemInLocalStorageByName = (name, val) =>{
    const item = Object.values(localStorage).find(value =>{
        let objValue = JSON.parse(value)
        if (objValue[name] === val){
            return objValue
        }
    })
    
    if (item !== undefined){
        return JSON.parse(item)
    }
}


const saveFormToLocalStorage = (elements) =>{
    let id = localStorage.getItem('id') === null ? 0 : localStorage.getItem('id')
    const objectLocal = {}
    for (let i in elements){
        let element = elements[i]
        let name = element.getAttribute('name')
        if (element.value.length < 1){
            alert('Введите данные')
            return
        }
        objectLocal[name] = element.value
        element.value = ''
    }
    localStorage.setItem(id, JSON.stringify(objectLocal))
    alert('Данные сохранены')
    id = Number(id) + 1
    localStorage.setItem('id', id)

}