window.onload = () =>{
    const menu = document.querySelector('#main_menu')
    const features = document.querySelector('#features')
    const toggleMenuBtn = document.querySelector('#toggle_menu')
    const resizeCallback = () =>{
        if (window.innerWidth <= 450){
            features.classList.add('feature_responsive')
        }
        console.log(window.innerWidth)
        toggleMenuBtn.onclick = () =>{
            if (window.innerWidth <= 900){
                console.log('here')
                menu.classList.toggle('responsive')
            }
        }
    }
    resizeCallback()
    window.addEventListener('resize', resizeCallback())
}

