const allImages = $('img')
allImages.click(function(){
    $(this).hide(3000)
    setTimeout(() => getRandomShow(this), 5000)
})



const getWidthEffect = (img) =>{
   $(img).show(1000)
   $(img).animate({
       width: 'hide'
   }, 2000)
   $(img).animate({
       width: 'show'
   }, 2000)
}

const getMarginEffect = (img) =>{
    $(img).animate({ marginLeft: '-=100px' }, 2000)
    $(img).show(1000)
    $(img).animate({ marginLeft: '+=100px' }, 2000)
}

const getOpacityEffect = (img) =>{
    $(img).show(1000)
    $(img).animate({ opacity: '-=1' }, 300)
    $(img).animate({ opacity: '+=1' }, 300)
    $(img).animate({ opacity: '-=1' }, 300)
    $(img).animate({ opacity: '+=1' }, 300)
}

const getRotateEffect = (img) => {
    $(img).show(1000)
    $(img).animate({  borderSpacing: -360 }, {
        step: function(now,fx) {
          $(this).css('-webkit-transform','rotate('+now+'deg)')
          $(this).css('-moz-transform','rotate('+now+'deg)')
          $(this).css('transform','rotate('+now+'deg)')
        },
        duration:'slow'
    },'linear')
}

const getPulseEffect = (img, n) => {
    $(img).show(1000)
    for (let i = 0; i<=n; i+=1){
        $(img).animate({ width: 'hide'}, 100)
        $(img).animate({ width: 'show'}, 100)
        $(img).animate({ height: 'hide'}, 100)
        $(img).animate({ height: 'show'}, 100)
        $(img).animate({ marginLeft: '-=100px' }, 100)
        $(img).animate({ marginLeft: '+=100px' }, 100)
    }
}


const getRandomShow = (img) =>{
    const arrRandEffects = ['getPulseEffect(img, 3)', 'getRotateEffect(img)', 'getWidthEffect(img)', 'getMarginEffect(img)', 'getOpacityEffect(img)']
    const index = Math.floor(Math.random() * arrRandEffects.length)
    const func = arrRandEffects[index]
    console.log(func)
    eval(func)
}